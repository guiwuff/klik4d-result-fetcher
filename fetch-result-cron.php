<?php
/**
 * Consume Result from stdLib endpoint using Guzzle.
 * Execute the files regularly using cron.
 */

require __DIR__ . '/vendor/autoload.php';
use GuzzleHttp\Client;

/**
 * Function to compare array.
 *
 * @param array $stored Array of stored result.
 * @param array $fetched Array of fetched result.
 * @return boolean true|false True if the fethed result has new result.
 */
function hasNewResult( $stored, $fetched ) {
    $index = 0;
    foreach($stored as $store) {
        echo "Comparing {$fetched[$index]['date']} and {$store['date']}";
        if ($fetched[$index]['date'] != $store['date']) {
            return true;
        }
        $index++;
    }
    return false;
}
$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'https://guiwuff.api.stdlib.com',
    // You can set any number of default request options.
    'timeout'  => 10.0,
]);

$response = $client->request('GET','k4dFetchResults@0.1.0');

// Proceed with save data to a json file
// if the http request is succesfull
// and the content inside the response is the results we are looking for

if ($response->getStatusCode() == '200') {
    // is the body have 7 results as expected
    $fetched       = $response->getBody();
    $fetched_arr   = json_decode($fetched, true);
    $fetched_count = count($fetched_arr);
    if ($fetched_count == 7) {
        $stored_file   = 'results.json';
        $stored_arr    = json_decode(file_get_contents($stored_file), true);
        // check if has new result
        if (hasNewResult($stored_arr, $fetched_arr)) {
            $fhandle = fopen('results.json', 'w');
            fwrite($fhandle,$response->getBody());
            fclose($fhandle);
            echo "New results updated";
        }
    }
}